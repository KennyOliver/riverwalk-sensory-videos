let video_display = document.getElementById("video-display");
let video_download_btn = document.getElementById("download-button");
let current_video_name = document.getElementById("currently-playing-name");
let current_thumbnail = document.getElementById("thumbnail-img");

function change_video(filename) {
  let filepath = `videos/${filename}.mp4`;
  let thumbnail = `images/${filename}.jpeg`;
  
  video_display.pause();
  
  video_display.src = filepath;
  // video_display.poster = thumbnail;
  video_download_btn.href = filepath;
  current_video_name.innerHTML = `<b>Currently playing:</b><br>"${filename}.mp4"`;
  current_thumbnail.src = thumbnail;
  
  video_display.load();
  video_display.play();
}

video_display.load(); //load first video